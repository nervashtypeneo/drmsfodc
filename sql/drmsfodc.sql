-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2019 at 02:26 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drmsfodc`
--

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `telephone` varchar(13) NOT NULL,
  `age` int(11) NOT NULL,
  `civil_status` varchar(20) NOT NULL,
  `occupation` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `firstname`, `lastname`, `address`, `telephone`, `age`, `civil_status`, `occupation`) VALUES
(1, 'Super', 'Pasaporte', 'greenfield Village, Inayawan Cebu', '1956483011', 25, 'taken', 'asdasda');

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

CREATE TABLE `records` (
  `records_id` int(11) NOT NULL,
  `id_patient` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `records_status` int(11) NOT NULL,
  `tooth_transaction` varchar(50) DEFAULT NULL,
  `cashonhand` double DEFAULT NULL,
  `cashonhand2` double DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `records`
--

INSERT INTO `records` (`records_id`, `id_patient`, `id_user`, `date`, `records_status`, `tooth_transaction`, `cashonhand`, `cashonhand2`, `notes`) VALUES
(1, 1, 4, '2019-10-06', 4, '', 200, 600, '            \r\n          asdasdas');

-- --------------------------------------------------------

--
-- Table structure for table `records_details`
--

CREATE TABLE `records_details` (
  `details_id` int(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `price` double NOT NULL,
  `id_service` int(11) NOT NULL,
  `amount` double NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `records_details`
--

INSERT INTO `records_details` (`details_id`, `service_name`, `price`, `id_service`, `amount`, `quantity`) VALUES
(1, 'asdsad', 577, 1, 577, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `service_description` text NOT NULL,
  `service_status` int(11) NOT NULL,
  `service_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_description`, `service_status`, `service_price`) VALUES
(1, 'asdsad', 'asdasdsa', 1, 577),
(2, 'Tooth Align', 'Align Tooth', 1, 800),
(3, 'Tooth Extraction', 'Extrace Tooth', 1, 550),
(4, 'Laser Filling', 'aSADSA', 1, 800),
(5, 'dsadsa', 'dasdsad', 2, 550);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `users_firstname` varchar(50) NOT NULL,
  `users_lastname` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `users_firstname`, `users_lastname`, `address`, `email`, `position`, `status`, `username`, `password`, `user_level`) VALUES
(1, 'alaiza', 'pasaporte', 'greenfield', 'cadburymonster16@gmail.com', 'super_admin', 0, 'super_admin', 'ed49c3fed75a513a79cb8bd1d4715d57', 0),
(2, 'dave', 'dave', 'davedave', 'Nurz_on_duty2009@yahoo.com', 'admin', 0, 'alaiza', '1e914900ab6841d0f2fde283784ad38c', 2),
(3, 'asdas', 'asdsa', 'asdasd', 'asdas@gmail.com', 'admin', 0, 'alaiza', '137adc556897a7bdf6fc89ffa308d8d1', 2),
(4, 'dentist', 'dentist', 'dentist', 'dentist@gmail.com', 'dentist', 0, 'dentist', '6cdbe1e638dd503cdf11f963dc283835', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`records_id`),
  ADD KEY `id_patient` (`id_patient`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `records_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `records`
--
ALTER TABLE `records`
  ADD CONSTRAINT `records_ibfk_1` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`patient_id`),
  ADD CONSTRAINT `records_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
