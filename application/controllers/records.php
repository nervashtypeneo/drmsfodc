<?php  
	class records extends CI_Controller{

		public function __construct(){

			parent::__construct();
			$this->load->model('patient_model');
			$this->load->model('services_model');
			$this->load->model('records_model');



		}

		public function view_list_patients(){

			$data['value'] =$this->patient_model->get_patient();

			$this->load->view('header/header');
			$this->load->view('Records/index',$data);
			$this->load->view('footer/footer');
		}

		public function view_patient_service($patient_id){

			$data['value'] = $this->patient_model->get_selected_patient($patient_id);

			$this->load->view('header/header');
			$this->load->view('Records/view_patient_service',$data);
			$this->load->view('footer/footer');

		}




	}?>