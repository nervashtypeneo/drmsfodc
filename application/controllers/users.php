<?php 

class users extends CI_Controller{

	public function __construct(){

		parent::__construct();

		$this->load->model('users_model');

	}

		public function super_admin(){

		$data['value'] = $this->users_model->get_all_users();	

		$this->load->view('header/header');

		$this->load->view('users/index',$data);

		$this->load->view('footer/footer');
		
	}


	public function index(){

		$data['value'] = $this->users_model->get_users();	

		$this->load->view('header/header');

		$this->load->view('users/index',$data);

		$this->load->view('footer/footer');
		
	}

	public function form_add_user(){

		$this->load->view('header/header');

		$this->load->view('users/form_add');

		$this->load->view('footer/footer');

	}

	public function insert(){

		$this->form_validation->set_rules('fname','fname','trim|required|is_unique[users.users_firstname]');
		$this->form_validation->set_rules('lname','lname','trim|required');
		$this->form_validation->set_rules('address','address','trim|required');
		$this->form_validation->set_rules('email','email','trim|required');
		$this->form_validation->set_rules('position','position','trim|required');
		$this->form_validation->set_rules('username','username','trim|required|is_unique[users.username]');
		$this->form_validation->set_rules('password','password','trim|required');

		if($this->form_validation->run()==FALSE){

			echo validation_errors();

		}else{

			$data = array(

			'users_firstname' => $this->input->post('fname'),	
			'users_lastname' => $this->input->post('lname'),	
			'address' => $this->input->post('address'),	
			'email' => $this->input->post('email'),	
			'position' => $this->input->post('position'),	
			'username' => $this->input->post('username'),	
			'password' => md5($_POST['password']),
			'user_level'=>2	

			);

			$result = $this->users_model->insert($data);

			if($result){

				$this->session->set_flashdata('success',"Account Added Successfully");

			}else{

				$this->session->set_flashdata('error',"Failed to insert");

			}

			if($this->session->userdata['logged_in']['position'] == "super_admin"){

			return redirect('users/super_admin');

		}else{

  			return redirect('users');
		}

		}

	}

	public function view_edit_form($user_id){

		$data['row'] = $this->users_model->get_acc_by_id($user_id);

		$this->load->view('header/header');

		$this->load->view('users/edit_account',$data);

		$this->load->view('footer/footer');

	}

	public function update($user_id){


		$this->form_validation->set_rules('users_firstname','firstname','trim|required');

		$this->form_validation->set_rules('users_lastname','lastname','trim|required');

		$this->form_validation->set_rules('address','address','trim|required');

		$this->form_validation->set_rules('email','email','trim|required');

		$this->form_validation->set_rules('position','position','trim|required');

		$this->form_validation->set_rules('username','username','trim|required');

		$this->form_validation->set_rules('password','password','trim|required');

		if($this->form_validation->run() == FALSE){

			echo validation_errors();

		}else{

			$data = array(
				'users_lastname' => $this->input->post('users_lastname'),
				'users_firstname' => $this->input->post('users_firstname'),
				'address'=>$this->input->post('address'),
				'email'=>$this->input->post('email'),
				'status'=>$this->input->post('status'),
				'position'=>$this->input->post('position'),
				'username'=>$this->input->post('username'),
				'password'=>md5($_POST['password']),
				'user_level' => 2
			);

			$result = $this->users_model->update_user($user_id,$data);

			if($result){

				$this->session->set_flashdata('success',"User has been updated");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}

		if($this->session->userdata['logged_in']['position'] == "super_admin"){

			return redirect('users/super_admin');

		}else{
			
  			return redirect('users');
		}

		}


	}

	public function disable_user($user_id){

		$data = array(

			'status' => 1
		);

		$result = $this->users_model->update_user($user_id,$data);

		if($result){

			$this->session->set_flashdata('success',"User has been Disable");

		}else{

			$this->session->set_flashdata('error',"Something went wrong please try again");
		}

		return redirect('users');
	}

	public function enable_user($user_id){

		$data = array(

			'status' => 0
		);

		$result = $this->users_model->update_user($user_id,$data);

		if($result){

			$this->session->set_flashdata('success',"User has been Enable");

		}else{

			$this->session->set_flashdata('error',"Something went wrong please try again");
		}

		return redirect('users');

	}


		public function disable($user_id){

		$data = array(

			'status' => 1
		);

		$result = $this->users_model->update_user($user_id,$data);

		if($result){

			$this->session->set_flashdata('success',"User has been Disable");

		}else{

			$this->session->set_flashdata('error',"Something went wrong please try again");
		}

		return redirect('users/super_admin');
	}

	public function enable($user_id){

		$data = array(

			'status' => 0
		);

		$result = $this->users_model->update_user($user_id,$data);

		if($result){

			$this->session->set_flashdata('success',"User has been Enable");

		}else{

			$this->session->set_flashdata('error',"Something went wrong please try again");
		}

		return redirect('users/super_admin');

	}

	public function update_info(){

				$user_id = $this->uri->segment(3);
				$data = array
				(
					'value' => $this->users_model->get_user($user_id)
				);
				$this->load->view('header/header');
				$this->load->view('users/account_settings',$data);
				$this->load->view('footer/footer');
	}

	public function update_account(){


		$this->form_validation->set_rules('firstname','firstname','trim|required');

		$this->form_validation->set_rules('lastname','lastname','trim|required');

		$this->form_validation->set_rules('address','address','trim|required');

		$this->form_validation->set_rules('email','email','trim|required');

		$this->form_validation->set_rules('username','username','trim|required');

		$this->form_validation->set_rules('password','password','trim|required');

		if($this->form_validation->run() == FALSE){

			echo validation_errors();

		}else{
			$user_id = $this->input->post('id');

			$data = array(
				'users_lastname' => $this->input->post('lastname'),
				'users_firstname' => $this->input->post('firstname'),
				'address'=>$this->input->post('address'),
				'email'=>$this->input->post('email'),
				'username'=>$this->input->post('username'),
				'password'=>md5($_POST['password']),
			);

			$result = $this->users_model->update_user($user_id,$data);

			if($result){

				$this->session->set_flashdata('success',"Account has been updated");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}

  		if($this->session->userdata['logged_in']['position'] == "super_admin"){

			return redirect('dashboard');

		}
		if($this->session->userdata['logged_in']['position'] == "admin"){

  			return redirect('dashboard');
		}if($this->session->userdata['logged_in']['position'] == "staff"){

			return redirect('dashboard/staff');
		}else{
			return redirect('cart/view_all_serve/1');
		}



	}
}




}
?>