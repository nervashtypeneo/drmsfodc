<?php

class cart extends CI_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->library('cart');
		$this->load->model('records_model');
	}

	public function add(){

		$data = array(

			'id'=>$this->input->post('service_id'),
			'name'=>$this->input->post('service_name'),
			'qty'=>1,
			'price'=>$this->input->post('service_price')
		);

		$result = $this->cart->insert($data);
		if($result){
				$this->session->set_flashdata('success',"Services Successfully Added");
		}else{
				$this->session->set_flashdata('error',"Oops! Something went wrong please try again later.");
		}
		return redirect('services/add_records');
	}

	public function remove($rowid){

		if($rowid === "all"){

			$this->cart->destroy();
			
		}else{

			$data = array(

				'rowid' => $rowid,
				'qty' => 0

			);
			$this->cart->update($data);
		}

		redirect('services/add_records');
	}

	public function addtoserve(){

		$patient_id = $this->input->post('patient_id');
		$data = array(
			'id_patient' => $patient_id,
			'records_status'=> '1',
			'date'=>date("Y-m-d")
		);
		$details_id = $this->records_model->insert_toserve($data);

		if($cart =$this->cart->contents()):
			foreach ($cart as $item):
				$records_detail = array(

					'details_id' => $details_id,
					'service_name' => $item['name'],
					'price' => $item['price'],
					'id_service'=>$item['id'],
					'quantity'=>$item['qty'],
					'amount'=>$item['subtotal']
				);

				$this->records_model->insert_records_detail($records_detail);

			endforeach;
		endif;
		$this->cart->destroy();
		redirect('cart/view_all_serve/1');

	}

	public function view_all_serve(){

		$stat = $this->uri->segment(3);
		$data = array(

			'all_service' => $this->records_model->get_all_service($stat),
			'count_toserve' => $this->records_model->count_toserve(),
			'count_serving' => $this->records_model->count_serving(),
			'count_topay' => $this->records_model->count_topay()
		);

		$this->load->view('header/header');
		$this->load->view('Records/list_tobe_serve',$data);
		$this->load->view('footer/footer');
	}


	public function view_service_detail(){

		$record_detail = $this->uri->segment(3);
		$data = array(

			'to_serve' => $this->records_model->get_records_detail($record_detail)
		);
		$this->load->view('header/header');
		$this->load->view('Records/records_detail',$data);
		$this->load->view('footer/footer');

	}

	public function confirm_service($details_id){

		$status = array(

			'records_status' => 2
		);
		$this->records_model->confirm_service($details_id,$status);

		redirect('cart/view_all_serve/2');

	}

	public function insert_precords($details_id){

		$id = $this->session->userdata['logged_in']['user_id'];
		$status = array(

			'id_user' => $id,
			'records_status' => 3,
			'tooth_transaction' => $this->input->post('extraction'),
			'notes'=>$this->input->post('notes')

		);

		$this->records_model->confirm_service($details_id,$status);

		redirect('cart/view_all_serve/1');

	}

	public function paid_patient($details_id){

		$status = array(

			'records_status' => 4
		);
		$this->records_model->confirm_service($details_id,$status);
		redirect('cart/view_all_serve/1');
		
		
	}

	public function add_coh($details_id){

		$status = array(

			'cashonhand' => $this->input->post('coh')
		);
		$this->records_model->confirm_service($details_id,$status);
		redirect('cart/view_all_serve/3');

	}

	public function add_coh2($details_id){

		$status = array(

			'cashonhand2' => $this->input->post('coh2')
		);
		$this->records_model->confirm_service($details_id,$status);
		redirect('cart/view_all_serve/3');

	}

}

 ?>