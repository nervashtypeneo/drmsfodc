<?php
class patient extends CI_Controller{

		public function __construct(){

				parent::__construct();
				$this->load->model('patient_model');
				$this->load->model('records_model');


		}

		public function index(){

			$data['value'] =$this->patient_model->get_patient();

			$this->load->view('header/header');
			$this->load->view('patient/index',$data);
			$this->load->view('footer/footer');
		}

		public function add(){

			$this->form_validation->set_rules('firstname','firstname','trim|required');
			$this->form_validation->set_rules('lastname','lastname','trim|required');
			$this->form_validation->set_rules('address','address','trim|required');
			$this->form_validation->set_rules('telephone','telephone','trim|required');
			$this->form_validation->set_rules('age','age','trim|required');
			$this->form_validation->set_rules('civil_status','civil_status','trim|required');
			$this->form_validation->set_rules('occupation','occupation','trim|required');

			if($this->form_validation->run() == FALSE){

				echo validation_errors();

			}else{

				$data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'address' => $this->input->post('address'),
					'telephone' => $this->input->post('telephone'),
					'age' => $this->input->post('age'),
					'civil_status' => $this->input->post('civil_status'),
					'occupation' => $this->input->post('occupation')
				);

				$result = $this->patient_model->insert($data);

				if($result){

						$this->session->set_flashdata('success',"Patient Added Successfully");

				}else{

					$this->session->set_flashdata('error',"Failed to insert");
				}

				return redirect('patient');

			}


		}

		public function edit_form($patient_id){
			$data['value'] = $this->patient_model->get_selected_patient($patient_id);
			$this->load->view('header/header');
			$this->load->view('patient/edit',$data);
			$this->load->view('footer/footer');

		}

		public function update($patient_id){

			$this->form_validation->set_rules('firstname','firstname','trim|required');
			$this->form_validation->set_rules('lastname','lastname','trim|required');
			$this->form_validation->set_rules('address','address','trim|required');
			$this->form_validation->set_rules('telephone','telephone','trim|required');
			$this->form_validation->set_rules('age','age','trim|required');
			$this->form_validation->set_rules('civil_status','civil_status','trim|required');
			$this->form_validation->set_rules('occupation','occupation','trim|required');

			if($this->form_validation->run() == FALSE){

				echo validation_errors();

			}else{
				$data = $this->input->post();

				unset($data['submit']);

			$result = $this->patient_model->update_data($patient_id,$data);

			if($result){

				$this->session->set_flashdata('sucess',"Patient has been updated");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}
			return redirect('patient');

		}


		}

		public function view_patients_profile($patient_id){

			$data = array(

				'value' => $this->patient_model->get_selected_patient($patient_id),
				'history' => $this->records_model->get_precords($patient_id)

			);
			$this->load->view('header/header');
			$this->load->view('patient/patients_profile',$data);
			$this->load->view('footer/footer');

		}

		public function get_records(){

		$record_detail = $this->uri->segment(3);
		$data = array(

			'history' => $this->records_model->get_records_detail($record_detail)
		);
		$this->load->view('header/header');
		$this->load->view('patient/records_detail',$data);
		$this->load->view('footer/footer');
		}





}
?>