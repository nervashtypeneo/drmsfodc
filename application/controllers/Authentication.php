<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Authentication extends CI_Controller{

 	public function __construct(){

 		parent:: __construct();

 		$this->load->model('Authentication_model');

 	}

 	public function index(){

 		$this->load->view('index.php');
 	}

 		public function login_process(){


		$this->form_validation->set_rules('username', 'username', 'trim|required');

		$this->form_validation->set_rules('password', 'password', 'trim|required');


		if($this->form_validation->run())

			$data = array(

			'username' => $this->input->post('username'),

			'password' => md5($_POST['password'])

			);	

			$result = $this->Authentication_model->login($data);


		if ($result == TRUE) {


			$username = $this->input->post('username');

			$result = $this->Authentication_model->read_user_info($username);


			$session_data = array(

			'user_id' => $result[0]->user_id,

			'username' => $result[0]->username,

			'email' => $result[0]->email,

			'users_firstname' =>$result[0]->users_firstname,

			'users_lastname'=>$result[0]->users_firstname,

			'address'=>$result[0]->address,

			'position'=>$result[0]->position,

			'password'=>$result[0]->password,

			'status'=>$result[0]->status

			);

				if($session_data['position'] == "super_admin"){
 					if($session_data['status'] == 1){

 					$this->session->set_flashdata('error','Your Account Has Been Disabled, Please Contact Your Administrator');

 					redirect('Authentication');
 					}else{

 						$this->session->set_userdata('logged_in',$session_data);
 						return redirect('dashboard');
 					}
 				}

 				if($session_data['position'] == "admin"){
 					if($session_data['status'] == 1){

 					$this->session->set_flashdata('error','Your Account Has Been Disabled, Please Contact Your Administrator');

 					redirect('Authentication');
 					}else{

 						$this->session->set_userdata('logged_in',$session_data);
 						return redirect('dashboard');
 					}

 				}if($session_data['position'] == "staff"){
 					if($session_data['status'] == 1){

 					$this->session->set_flashdata('error','Your Account Has Been Disabled, Please Contact Your Administrator');
 					redirect('Authentication');

 					}else{

 						$this->session->set_userdata('logged_in',$session_data);
 						return redirect('dashboard/staff');
 						
 					}
 				}if($session_data['position'] == "dentist"){
 					if($session_data['status'] == 1){

 					$this->session->set_flashdata('error','Your Account Has Been Disabled, Please Contact Your Administrator');

 					redirect('Authentication');
 					}else{

 						$this->session->set_userdata('logged_in',$session_data);
 						return redirect('cart/view_all_serve/1');

 					}
 				}

 			}else{

 			$this->session->set_flashdata('error','Invalid Username or Password');
			redirect('Authentication');

 			}

 		}

 		public function logout(){

			session_destroy();
			redirect('Authentication');

		}


		public function view_forgot(){
			$this->load->view('Forgot Password');
		}

		public function forget_password(){

			$email =$this->input->post('email');


			   // get the user_info array row
		    $query1 = $this->db->query("SELECT * from users where email = '" . $email . "'");
		    $row = $query1->result_array();
		    if ($query1->num_rows() > 0) {


		        // assign users name to a variable
		        // $full_name = $row['firstname'];
		        // // generate password from a random integer

		        $this->load->helper('string');
				$passwordplain= random_string('alnum', 16);


		        // $passwordplain = rand(999999999, 9999999999);
		        // // encrypt password
		        $encrypted_pass = $this->pass_gen($passwordplain);
		        $newpass['password'] = $encrypted_pass;
		        // update password in db
		        $this->db->where('email', $email);
		        $this->db->update('users', $newpass);
		    // begin email functions
		    $result = $this->email_user($email, $passwordplain);

			}else{

				$this->session->set_flashdata('error','The email you entered was not found on our database');
			redirect('Authentication/view_forgot');

			}

		}

	public function email_user($email, $passwordplain) {

		echo $email,$passwordplain;

		 $config = array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'ssl://smtp.googlemail.com',
		  'smtp_port' => 465,
		  'smtp_user' => 'clinicorthodental@gmail.com',
		  'smtp_pass' => 'laiza_31',
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1'
		);

		$mail_message .= 'Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>' . $passwordplain . '</b>' . "\r\n";
	    $mail_message .= '<br>Please Update your password.';
	    $mail_message .= '<br>Thanks & Regards';
	    $mail_message .= '<br>Your company name';

          $this->load->library('email', $config);
	      $this->email->set_newline("\r\n");
	      $this->email->from('');
	      $this->email->to($email);
	      $this->email->subject('Forget Password');
	      $this->email->message($mail_message);
	      if($this->email->send()){

	      	$this->session->set_flashdata('success','Password has been reset and has been sent to your email :'.$email);
			 redirect('Authentication/view_forgot');
	      }else{

	      	show_error($this->email->print_debugger());

	      }

}

// Password encryption
public function pass_gen($password) {
    $encrypted_pass = md5($password);
    return $encrypted_pass;
}


	


}?>