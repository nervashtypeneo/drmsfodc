<?php

class services extends CI_Controller{

	public function __construct(){

		parent::__construct();
		$this->load->model('services_model');
	}

	public function add_records(){

			$data['service']=$this->services_model->get_all_services();
			$this->load->view('header/header');
			$this->load->view('Records/add_records',$data);
			$this->load->view('footer/footer');

	}

	public function list_services(){

		$data['value'] = $this->services_model->get_services();

		$this->load->view('header/header');

		$this->load->view('services/index',$data);

		$this->load->view('footer/footer');

	}

	public function insert(){

		$this->form_validation->set_rules('sname','sname','trim|required|is_unique[services.service_name]');
		$this->form_validation->set_rules('descript','descript','trim|required');
		$this->form_validation->set_rules('status','status','trim|required');
		$this->form_validation->set_rules('price','price','trim|required');	

		if($this->form_validation->run()==FALSE){

			echo validation_errors();

		}else{

			$data = array(

			'service_name' => $this->input->post('sname'),
			'service_description' => $this->input->post('descript'),
			'service_status' => $this->input->post('status'),
			'service_price' => $this->input->post('price')

			);

			$result = $this->services_model->insert($data);

			if($result){

				$this->session->set_flashdata('success',"Services successfully added");

			}else{

				$this->session->set_flashdata('error',"Failed to insert");
			}

			return redirect('services/list_services');

		}

	}

	public function edit_form($service_id){

		$data['value'] =$this->services_model->get_service_by_id($service_id);
		$this->load->view('header/header');
		$this->load->view('services/update',$data);
		$this->load->view('footer/footer');

	}

	public function update_service($service_id){

		$this->form_validation->set_rules('service_name','service_name','trim|required');
		$this->form_validation->set_rules('service_description','service_description','trim|required');
		$this->form_validation->set_rules('service_status','service_status','trim|required');
		$this->form_validation->set_rules('service_price','service_price','trim|required');

		if($this->form_validation->run()==FALSE){

			echo validation_errors();

		}else{

			$data = $this->input->post();

			unset($data['submit']);

			$result = $this->services_model->update_data($service_id,$data);

			if($result){

				$this->session->set_flashdata('success',"Service has been updated");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}
			return redirect('services/list_services');


		}


	}

	public function disable($service_id){

		$data = array(

			'service_status' =>2

		);
		$result = $this->services_model->update_data($service_id,$data);
		if($result){

				$this->session->set_flashdata('success',"Service has been disable");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}
			return redirect('services/list_services');


		}


	public function enable($service_id){

		$data = array(

			'service_status' =>1

		);
		$result = $this->services_model->update_data($service_id,$data);
		if($result){

				$this->session->set_flashdata('success',"Service has been enable");

			}else{

				$this->session->set_flashdata('error',"Something went wrong please try again");

			}
			return redirect('services/list_services');


		}









	
}
?>