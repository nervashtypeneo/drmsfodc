<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Patient</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                               <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <?php echo form_open('patient/add'); ?>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Firstname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="firstname" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"  />
                                                                </div>
                                                            </div>
                                                        </div>

                                                      <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Lastname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="lastname" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Address</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="address" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Telephone</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="number" name="telephone" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Age</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="number" name="age" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==4) return false;" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                         <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Status</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="civil_status" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                              <br/>

                                                         <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Occupation</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="occupation" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/><br/>

                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-6"></div>
                                                                    <div class="col-lg-6">
                                                                        <div class="login-horizental cancel-wp pull-left">
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                                            
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
      </div>
    </div>
  </div>
</div>
