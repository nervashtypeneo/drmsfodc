
                        <div class="col-lg-12">
                            <div class="sparkline12-list shadow-reset mg-t-30">
                                <div class="sparkline12-hd">
                                    <div class="main-sparkline12-hd">
                                        <h1>Edit Patient</h1>
                                    </div>
                                </div>



 <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <?php echo form_open("patient/update/{$value->patient_id}"); ?>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Firstname</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->firstname; ?>" name="firstname" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                            <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Lastname</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->lastname; ?>" name="lastname" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Address</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->address; ?>" name="address" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Telephone</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="number" value="<?php echo $value->telephone; ?>" name="telephone" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                                <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Age</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                     <input value="<?php echo $value->age; ?>" name="age" type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==2) return false;" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                         <br/>

                                                         <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Status</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->civil_status; ?>" name="civil_status" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Occupation</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->occupation; ?>" name="occupation" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>


                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-5"></div>
                                                                    <div class="col-lg-2">
                                                                        <div class="login-horizental cancel-wp pull-right">
                                                                            <a href="<?php echo base_url('patient'); ?>" class="btn btn-danger">Cancel</a>
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
