<style type="text/css">
  
  .modal-header{
    color:#fff;
    background-color: #428bca;
  }    
</style>

        <div class="container-fluid">

                      <h1 class="h3 mb-2 text-gray-800">Patient List</h1>

          
                                  <?php if($success = $this->session->flashdata('success')): ?>
                                <div class="alert alert-success success-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $success; ?></strong></p>
                                </div>
                              <?php endif; ?>

                                        <?php if($error = $this->session->flashdata('error')): ?>
                                <div class="alert alert-danger alert-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $error; ?></strong></p>
                                </div>
                              <?php endif; ?>

 <div class="card shadow mb-4">
            <div class="card-header py-3">
              <?php if(isset($this->session->userdata['logged_in'])): ?>

              <?php if($this->session->userdata['logged_in']['position'] == "admin" || $this->session->userdata['logged_in']['position'] == "staff" || $this->session->userdata['logged_in']['position'] == "super_admin" ) { ?>
              <a href="" data-toggle="modal" style="text-align: center;" data-target ='#exampleModal' class="btn btn-primary"><i class="far fa-plus-square"></i>Add Patient </a>
            <?php } ?>
                      <?php endif; ?>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                        <tr>
                         <th align="center" class="table-active">ID</th>
                         <th align="center">Fullname</th>
                         <th align="center">Address</th>
                         <th align="center">Action</th>
                    </tr>
                 </thead>
                 <tbody>

                        <?php if(!empty($value)): ?>
                            <?php foreach ($value as $row): ?>      
                    <tr>
                        <td align="center"><?php echo $row->patient_id; ?></td>

                        <td align="center"><?php echo $row->firstname." ".$row->lastname; ?></td>

                        <td align="center"><?php echo $row->address; ?></td>

                            <td align="center">

                                   

                                     <a href="<?php echo base_url('patient/view_patients_profile/'.$row->patient_id); ?>" class="btn btn-info">
                                    <i class="far fa-eye"></i>
                                   </a> 



                                    <a href="<?php echo base_url('patient/edit_form/'.$row->patient_id); ?>" class="btn btn-success">
                                    <i class="fas fa-user-edit"></i>
                                   </a> 


                                </td>

                      </tr>

                            <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td>No Records Found</td>
                            </tr>
                            <?php endif; ?>
                            </tr>
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>




                 </tbody>
             </table>
         </div>
     </div>
 </div>


  <script type="text/javascript">

    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 3000);


  </script>

 <?php $this->load->view('patient/add_modal'); ?>


