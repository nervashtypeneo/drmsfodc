<div class="container">
		         <div class="card shadow mb-5">
            <div class="card-body">
  <div class="row">
    <div class="col-sm">
      Firstname : <text style="color: green;"><?php echo $value->firstname ?></text> 
    </div>
    <div class="col-sm">
      Lastname : <text style="color: green;"><?php echo $value->lastname ?></text>
    </div>
    <div class="col-sm">
      Address : <text style="color: green;"><?php echo $value->address ?></text>
    </div>
  </div><br/>
    <div class="row">
    <div class="col-sm">
      Telephone #: <text style="color: green;"><?php echo $value->telephone ?></text>
    </div>
    <div class="col-sm">
      Age : <text style="color: green;"><?php echo $value->age; ?></text>
    </div>
    <div class="col-sm">
     Civil Status : <text style="color: green;"><?php echo $value->civil_status; ?></text>
    </div>
  </div><br/>
  <div class="row">
  	<div class="col-sm">
  		Occupation : <text style="color: green;"><?php echo $value->occupation; ?></text>
  	</div>
  </div>
</div>
<br/>
<br/>
<br/>
<center><h3>Dental Records</h3></center>
		<div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                        <tr>
                         <th align="center">Assisted By</th>
                         <th align="center">Date</th>
                         <th align="center">Action</th>
                    </tr>
                 </thead>
                 <tbody>
                  <?php if(!empty($history)): ?>
                  <?php foreach($history as $row): ?>
                    <tr>
                    <td align="center"><?php echo $row->users_firstname. " ". $row->users_lastname; ?></td>
                    <td align="center"><?php echo $row->date; ?></td>
                                <!-- <td align="center"><?php echo $row->tooth_transaction; ?></td> -->
                    
                    <td align="center">
                      <a class="btn btn-primary" href="<?php echo base_url('patient/get_records/'.$row->records_id); ?>" >View More Details</a>
                    </td>
                    </tr>
                  <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <td>No records Found!</td>
                  </tr>
                <?php endif; ?>

                            </tbody>
                          </table>
                        </div>
                      </div>



</div>
</div>
</div>