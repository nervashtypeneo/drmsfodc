        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><?php echo $value->firstname; ?></h1>

          <div class="col-lg-8 col-md-8">
  <div id="cart_details">
   <h3 align="center">Items in Cart</h3>
  </div>

  <table cellpadding="10" cellspacing="2" style="width:100%" border="0">

<tr>
        <th>#</th>
        <th>Service</th>
        <th style="text-align:right">Item Price</th>
        <th style="text-align:right">Sub-Total</th>
</tr>

<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

        <tr>
                <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '1', 'size' => '1' , 'disabled'=>'true', 'class'=>'form-control-plaintext')); ?></td>
                <td>
                        <?php echo $items['name']; ?>

                        <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                <p>
                                        <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                                                <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

                                        <?php endforeach; ?>
                                </p>

                        <?php endif; ?>

                </td>
                <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
                <td style="text-align:right">&#8369;<?php echo $this->cart->format_number($items['subtotal']); ?></td>
        </tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
        <td colspan="2"> </td>
        <td class="right"><strong>Total</strong></td>
        <td class="right">&#8369;<?php echo $this->cart->format_number($this->cart->total()); ?></td>


</tr>

</table>
 </div>
        </div>
        <form method="post" action="<?php echo base_url('cart/addtoserve'); ?>" class="form-conrol">
          <input type="hidden" name="patient_id" value="<?php echo $value->patient_id; ?>">
       
    
        <button type="submit" class="btn btn-success">Proceed</button>
      </form>