<style type="text/css">
  
  .modal-header{
    color:#fff;
    background-color: #428bca;
  }    
</style>

        <div class="container-fluid">

                      <h1 class="h3 mb-2 text-gray-800">Patient's List</h1>
 <div class="card shadow mb-4">
            <div class="card-header py-3">


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                        <tr>

                         <th align="center">Lastname</th>
                         <th align="center">Firstname</th>
                         <th align="center">Action</th>
                    </tr>
                 </thead>
                 <tbody>
                        <?php if(!empty($value)): ?>
                            <?php foreach ($value as $row): ?>      
                    <tr>

                        <td align="center"><?php echo $row->firstname;?></td>

                        <td align="center"><?php echo $row->lastname; ?></td>

                          <td align="center">

                              <a href="<?php echo base_url('records/view_patient_service/'.$row->patient_id); ?>" class="btn btn-info" title="Add"><i class="fas fa-plus-circle"></i></button>

                            </td>

                                </tr>

                            <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td>No Records Found</td>
                            </tr>
                            <?php endif; ?>
                            </tr>
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>




                 </tbody>
             </table>
         </div>
     </div>
 </div>
