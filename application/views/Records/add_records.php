<style type="text/css">
  #container{
    float: left;
  }

</style>


                                  <?php if($success = $this->session->flashdata('success')): ?>
                                <div class="alert alert-success success-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $success; ?></strong></p>
                                </div>
                              <?php endif; ?>

                                        <?php if($error = $this->session->flashdata('error')): ?>
                                <div class="alert alert-danger alert-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $error; ?></strong></p>
                                </div>
                              <?php endif; ?>

<?php if(count($service)):?>


<div class="row">
        <?php foreach($service as $post):?> 
  <div class="col-sm-5">
    <div class="card text-white bg-primary mb-1" style="max-width: 30rem;">
  <div class="card-body">
    <h4 class="card-title"><?php echo $post->service_name?></h4>
    <p class="card-text"></p>
      </div>
      <?php echo form_open('cart/add'); ?>

      <input type="hidden" name="service_id" value="<?php echo $post->service_id; ?>">
      <input type="hidden" name="service_name" value="<?php echo $post->service_name; ?>">
      <input type="hidden" name="service_price" value="<?php echo $post->service_price; ?>">
      <center>
      <button type="submit" class="btn btn-success" >Add Service</button>
      </center>
      <?php echo form_close(); ?>
    </div>
  </div>
<?php endforeach; ?>
</div>


<?php else: ?>
<?php endif;?>

 <div class="col-lg-8 col-md-8">
  <div id="cart_details">
   <h3 align="center">Service</h3>
  </div>

  <table cellpadding="10" cellspacing="2" style="width:100%" border="0">

<tr>
        <th>#</th>
        <th>Item Description</th>
        <th style="text-align:right">Unit Price</th>
        <th style="text-align:right">Sub-Total</th>
        <th style="text-align: right">Action</th>
</tr>

<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>

        <tr>
                <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '1' , 'disabled'=>'true')); ?></td>
                <td>
                        <?php echo $items['name']; ?>

                        <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                <p>
                                        <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

                                                <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

                                        <?php endforeach; ?>
                                </p>

                        <?php endif; ?>

                </td>
                <td style="text-align:right" class="price"><?php echo $this->cart->format_number($items['price']); ?></td>
                <td style="text-align:right">&#8369;<?php echo $this->cart->format_number($items['subtotal']); ?></td>
                <td style="text-align: right"><a href="<?php echo base_url('cart/remove/'.$items['rowid']); ?>" class="btn btn-danger"><i class="fas fa-times-circle"></i></a></td>
        </tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
        <td colspan="3"> </td>
        <td class="right"><strong>Total</strong></td>
        <td class="right">&#8369;<?php echo $this->cart->format_number($this->cart->total()); ?></td>

</tr>

</table>
  <input type="button" class ='btn btn-danger' value="Clear Cart" onclick="clear_cart()">
  <input type="button" class="btn btn-success" value="Procced to Patient" onclick="to_patient()">
 </div>

</div>

  <script type="text/javascript">

    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 3000);

    function clear_cart(){
      var result = confirm('Are you sure want to clear all Services?');

      if(result){
        window.location="<?php echo base_url(); ?>cart/remove/all";
      }else{
        return false;
      }
    }

    function to_patient(){
      window.location="<?php echo base_url(); ?>records/view_list_patients";
    }

    $('#price').html('Php '+ x);


  </script>
