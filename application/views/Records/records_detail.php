;  <div class="container">
                <table class="table borderless">
                  <thead>
                        <tr>
                         <th>Dental Treatment</th>
                         <th>Quantity</th>
                         <th>Unit Price</th>
                         <th>Subtotal</th>
                         
                    </tr>
                 </thead>
                 <tbody>
                    <?php $sum = 0; ?>
                    <?php $changes = 0; ?>=0
                    <?php $cashes=0 ?>

                            <?php if(!empty($to_serve)): ?>

                             <?php foreach($to_serve as $row): ?>   
                            <tr>  
                                <td><?php echo $row->service_name; ?></td>
                                <td ><?php echo $row->quantity; ?></td>
                                <td>₱<?php echo $row->price; ?></td>
                                <td><?php echo $row->amount; ?></td>
                                <?php $sum = $sum+$row->amount  ; ?>
                            </tr>
                            <?php endforeach; ?>
                            Date:&nbsp<?php echo $row->date;?>
                                <div class="row">
                                  <div class="col-sm">
                                    
                                    <hr></hr>
                            <strong>
                            <h1><center><i class="fas fa-tooth"></i>&nbspOrtho Dental Clinic</h1></center></strong>
                            <center>Clinic Hours 9:00AM - 5:00PM</center>
                            <center>Contact No: 514-2630</center>
                            <center>Address: Punta Princesa Taurus st. Labangon Cebu City</center>
                            <center>Email: OrthoDental@gmail.com</center>
                            <hr></hr>
                          </div>


                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <br>
                          <tr>
                         <!-- <div class="row">
                          <div class="col">-->
                          <strong>BILL TO :</strong>
                          <br>
                          <br>
                          <strong>Patient Name:</strong> &nbsp&nbsp<text><?php echo $row->firstname. " " . $row->lastname;?></text> 
                          </div>
                          <!--<div class="col">-->
                            <br>
                          <strong>Address:</strong> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<text><?php echo $row->address; ?></text>
                          </div> 
                        </div>
                        <br/>
                        <!--<div class="row">
                          <div class="col">-->
                            <strong>Age:</strong>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<text><?php echo $row->age; ?></text>
                          </div>
                         <!-- <div class="col">-->
                          <br>
                            <strong>Contact Number:</strong>&nbsp&nbsp<text><?php echo $row->telephone; ?></text>
                          </div>
                        </div>
                          </tr>
                          <br>
                          <br>
                          <br>
                        </div><br>
                            <tr>
                                <td>

                                    <?php if(isset($this->session->userdata['logged_in'])): ?>
                                    <?php if($this->session->userdata['logged_in']['position'] == "dentist") { ?> 

                                    <?php if($row->records_status == 1) { ?>
                                        <button class="btn btn-primary" onclick="window.location='<?php echo base_url('cart/confirm_service/'.$row->details_id); ?>'">Accept</button>
                                    <?php } ?>

                                    <?php if($row->records_status == 2) { ?>
                                    <br>
                                    <strong><p style="float: right">Total: ₱<?php echo $sum; ?></p></strong>
                                    <br>
                                    <br>
                                    <div class="row">
                                      
                                    <form method="post" action="<?php echo base_url('cart/insert_precords/'.$row->details_id); ?>">
                                    <label>Note: For Tooth Extraction only</label><br>
                                    <div class="col-sm">
            <select name="extraction">
            <option value="">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp--Upper Tooth--</option>
            <option value="1. 3rd Molar (wisdom tooth)">1. 3rd Molar (wisdom tooth)</option>
            <option value="2. 2nd Molar (12-yr molar)">2. 2nd Molar (12-yr molar)</option>
            <option value="3. 1st Molar (6-yr molar)">3. 1st Molar (6-yr molar)</option>
            <option value="4. 2nd Bicuspid (2nd premolar)">4. 2nd Bicuspid (2nd premolar)</option>
            <option value="5. 1st Bicuspid (1st premolar)">5. 1st Bicuspid (1st premolar)</option>
            <option value="6. Cuspid (canine/eye tooth)">6. Cuspid (canine/eye tooth)</option>
            <option value="7. Lateral incisor ">7. Lateral incisor </option>
            <option value="8. Central incisor ">8. Central incisor </option>
            <option value="9. Central incisor">9. Central incisor</option>
            <option value="10. Lateral incisor">10. Lateral incisor</option>
            <option value="11. Cuspid (canine/eye tooth)">11. Cuspid (canine/eye tooth)</option>
            <option value="12. 1st Bicuspid (1st premolar)">12. 1st Bicuspid (1st premolar)</option>
            <option value="13. 2nd Bicuspid (2nd premolar)">13. 2nd Bicuspid (2nd premolar)</option>
            <option value="14. 1st Molar (6-yr molar)">14. 1st Molar (6-yr molar)</option>
            <option value="15. 2nd Molar (12-yr molar)">15. 2nd Molar (12-yr molar)</option>
            <option value="16. 3rd Molar (wisdom tooth)">16. 3rd Molar (wisdom tooth)</option>
            <option value="">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp--Lower Tooth--</option>
            <option value="17. 3rd Molar (wisdom tooth)">17. 3rd Molar (wisdom tooth)</option>
            <option value="18. 2nd Molar (12-yr molar)">18. 2nd Molar (12-yr molar)</option>
            <option value="19. 1st Molar (6-yr molar)">19. 1st Molar (6-yr molar)</option>
            <option value="20. 2nd Bicuspid (2nd premolar)">20. 2nd Bicuspid (2nd premolar)</option>
            <option value="21. 1st Bicuspid (1st premolar)">21. 1st Bicuspid (1st premolar)</option>
            <option value="22. Cuspid (canine/eye tooth)">22. Cuspid (canine/eye tooth)</option>
            <option value="23. Lateral incisor ">23. Lateral incisor </option>
            <option value="24. Central incisor ">24. Central incisor </option>
            <option value="25. Central incisor">25. Central incisor</option>
            <option value="26. Lateral incisor">26. Lateral incisor</option>
            <option value="27. Cuspid (canine/eye tooth)">27. Cuspid (canine/eye tooth)</option>
            <option value="28. 1st Bicuspid (1st premolar)">28. 1st Bicuspid (1st premolar)</option>
            <option value="29. 2nd Bicuspid (2nd premolar)">29. 2nd Bicuspid (2nd premolar)</option>
            <option value="30. 1st Molar (6-yr molar)">30. 1st Molar (6-yr molar)</option>
            <option value="31. 2nd Molar (12-yr molar)">31. 2nd Molar (12-yr molar)</option>
            <option value="32. 3rd Molar (wisdom tooth)">32. 3rd Molar (wisdom tooth)</option>
          </select>
        </div>
        <br/>
       <div class="col">
          <textarea class="form-control" name="notes" maxlength="100" rows="3">
            
          </textarea>
        </div>
          <br>
          <br>
                                        <button type="submit" class="btn btn-primary">Done</button>
                                       
                                    </form>  
                                  </div>
                                    <?php } ?>


                            <?php  } ?>
                             <?php endif; ?>
                         </td>


                     </tr>

                        <?php endif; ?>

                        

                </tbody>
            </table>
           

            <?php if(isset($this->session->userdata['logged_in'])): ?>
            <?php if($this->session->userdata['logged_in']['position'] != "dentist" ) { ?> 

            <?php $cashes = $row->cashonhand+$row->cashonhand2 ?>
           
  <?php if($row->records_status == 3) {  ?>
              <div class="row" style="text-align: right;">
                <div class="col-sm-9">
        <p><strong>Total Bill: ₱<?php echo $sum; ?></strong></p>
        <p><strong>Cash On Hand: ₱<?php echo $cashes; ?></strong></p>
        <?php $changes = $cashes - $sum; ?>
        <p><strong>Changes: ₱<?php echo $changes; ?></strong></p>
      </div>
    </div>
  <?php } ?> 
  

                  <?php if($row->records_status == 3) {  ?>
                     <div class="row" style="text-align: right;">
                    <div class="col-sm-10">
                    <p class="no-display"> Prepared By :&nbsp <?php echo $this->session->userdata['logged_in']['users_firstname'];?></p>
                  </div>
                </div>
         <button class="btn btn-primary no-print" id="printbill">Print Bill</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Payment
</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2">
  Payment 2
</button>

                                    <?php } ?>
                                    <?php } ?>
                                  <?php endif; ?>


  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="sum">Bill</label>
      <input class="form-control" type="disable" name="sum" value="<?php echo $sum ?>"><br/>
      <?php echo form_open("cart/add_coh/{$row->details_id}") ?>
      <label for="coh">Cash On Hand</label>
      <input class="form-control" type="number" name="coh"><br/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="sum">Bill</label>
      <input class="form-control" type="disable" name="sum" value="<?php echo $sum ?>"><br/>
      <?php echo form_open("cart/add_coh2/{$row->details_id}") ?>
      <label for="coh">Cash On Hand</label>
      <input class="form-control" type="number" name="coh2"><br/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>




     

</div>
</div>

 <script type="text/javascript">
  $(document).ready(function(){
    $('#printbill').click(function(){
      $.ajax({
        url: "<?php echo base_url('cart/paid_patient/'.$row->details_id); ?>",
        success:function(){
          window.print();
        }
      });

    });


  });


</script>