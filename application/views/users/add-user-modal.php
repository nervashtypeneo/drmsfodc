<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                               <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <?php echo form_open('users/insert'); ?>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Firstname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                   <span class="badge badge-danger"><?php echo form_error('fname'); ?></span>
                                                                    <input type="text" name="fname" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"  />
                                                                </div>
                                                            </div>
                                                        </div>

                                                      <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Lastname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                   <span class="badge badge-danger"><?php echo form_error('lname'); ?></span>
                                                                    <input type="text" name="lname" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" required />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                   <span class="badge badge-danger"><?php echo form_error('address'); ?></span>

                                                                    <label class="login2 pull-right pull-right-pro">Address</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="address" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                   <span class="badge badge-danger"><?php echo form_error('email'); ?></span>

                                                                    <label class="login2 pull-right pull-right-pro">Email</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="email" name="email" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Position</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                   <span class="badge badge-danger"><?php echo form_error('position'); ?></span>

                                                                    <div class="form-select-list">
                                                                        <?php if(isset($this->session->userdata['logged_in'])): ?>
                                                                        <?php if($this->session->userdata['logged_in']['position'] == "super_admin") { ?>
                                                                        <select class="form-control custom-select-value" name="position">
                                                                            <option>admin</option>
                                                                            <option>staff</option>
                                                                            <option>dentist</option>
                                                                        </select>
                                                                    <?php } ?>
                                                                    <?php if($this->session->userdata['logged_in']['position'] == "admin") { ?>
                                                                        <select class="form-control custom-select-value" name="position">
                                                                            <option>staff</option>
                                                                            <option>dentist</option>
                                                                        </select>
                                                                    <?php } ?>
                                                                    <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Username</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                   <span class="badge badge-danger"><?php echo form_error('username'); ?></span>
                                                                    <input type="text" name="username" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Password</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                   <span class="badge badge-danger"><?php echo form_error('password'); ?></span>
                                                                    <input type="password" name="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/><br/>

                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-6"></div>
                                                                    <div class="col-lg-6">
                                                                        <div class="login-horizental cancel-wp pull-left">
                                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                                            
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
      </div>
    </div>
  </div>
</div>
