
                        <div class="col-lg-12">
                            <div class="sparkline12-list shadow-reset mg-t-30">
                                <div class="sparkline12-hd">
                                    <div class="main-sparkline12-hd">
                                        <h1>Edit User</h1>
                                    </div>
                                </div>


                                <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                            <?php echo form_open("users/update/{$row->user_id}"); ?>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Firstname</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="text" name="users_firstname" value="<?php echo $row->users_firstname; ?>" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"  />
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Lastname</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="text" name="users_lastname" value="<?php echo $row->users_lastname; ?>" class="form-control" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)"  />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Address</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="text" name="address" value="<?php echo $row->address; ?>" class="form-control"   />
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Email</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="email" name="email" value="<?php echo $row->email; ?>" class="form-control"  />
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Position</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <div class="form-select-list">
                                                                        <select class="form-control custom-select-value" name="position">
                                                                            <option value="admin">Admin</option>
                                                                            <option value="staff">Staff</option>
                                                                            <option value="dentist">Dentist</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Username</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="text" name="username" value="<?php echo $row->username; ?>" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Password</label>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <input type="password" name="password" value="<?php echo $row->password; ?>" class="form-control" />
                                                                </div>
                                                            </div>
                                                        </div>

 

                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-1"></div>
                                                                    <div class="col-lg-3">
                                                                        <div class="login-horizental cancel-wp pull-left">
                                                                            <a href="<?php echo base_url('users'); ?>" class="btn btn-danger">Cancel</a>
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!-- Basic Form End-->