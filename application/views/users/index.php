<style type="text/css">
  
  .modal-header{
    color:#fff;
    background-color: #428bca;
  }    



</style>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">User List</h1>

                                  <?php if($success = $this->session->flashdata('success')): ?>
                                <div class="alert alert-success success-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $success; ?></strong></p>
                                </div>
                              <?php endif; ?>



                                <?php if($error = $this->session->flashdata('error')): ?>
                                <div class="alert alert-danger alert-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $error; ?></strong></p>
                                </div>
                              <?php endif; ?>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="" data-toggle="modal" style="text-align: center;" data-target ='#exampleModal' class="btn btn-primary"><i class="far fa-plus-square"></i>Add User </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                        <tr>
                         <th class="table-primary" align="center">ID</th>
                         <th align="center">Firstname</th>
                         <th align="center">Lastname</th>
                         <th align="center">Email</th>
                         <th align="center">Position</th>
                         <th align="center">Status</th>
                         <th align="center">Action</th>
                    </tr>
                 </thead>
                 <tbody>
                            <?php if(!empty($value)): ?>

                             <?php foreach($value as $row): ?>   
                            <tr>
                                <td class="table-success" align="center"><?php echo $row->user_id; ?></td>
                                <td align="center"><?php echo $row->users_firstname; ?></td>
                                <td align="center"><?php echo $row->users_lastname; ?></td>
                                <td align="center"><?php echo $row->email; ?></td>
                                <td align="center"><?php echo $row->position; ?></td>
                                <td align="center">
                            <?php if($row->status == 0) { ?>

                             <span class="badge badge-success">Active</span>

                         <?php } ?>

                         <?php if($row->status == 1) { ?>

                            <span class="badge badge-warning">In-active</span>

                         <?php } ?>

                                </td>

                                <td align="center">

                                    <a href="#" data-toggle="modal" data-target="#viewModal<?php echo $row->user_id; ?>" class="btn btn-info">
                                    <i class="far fa-eye"></i>
                                   </a> 

                                   <a href="<?php echo base_url('users/view_edit_form/'.$row->user_id); ?>" class="btn btn-success">
                                    <i class="fas fa-user-edit"></i>
                                   </a>
                            <?php if(isset($this->session->userdata['logged_in'])): ?>
                                   <?php if($row->status == 0) { ?>
                                    
                        <?php if($this->session->userdata['logged_in']['position'] == "super_admin") { ?>
                          <a href="<?php echo base_url('users/disable/'.$row->user_id) ?>" class="btn btn-danger">
                                    <i class="fas fa-toggle-off"></i>
                                    </a>
                              <?php } ?>
                               <?php if($this->session->userdata['logged_in']['position'] == "admin") { ?>
                          <a href="<?php echo base_url('users/disable_user/'.$row->user_id) ?>" class="btn btn-danger">
                                    <i class="fas fa-toggle-off"></i>
                                    </a>
                              <?php } ?>

                                    <?php } ?>

                                    <?php if($row->status == 1) { ?>

                          <?php if($this->session->userdata['logged_in']['position'] == "super_admin") { ?>
                          <a href="<?php echo base_url('users/enable/'.$row->user_id) ?>" class="btn btn-success">
                                    <i class="fas fa-toggle-off"></i>
                                    </a>
                              <?php } ?>
                               <?php if($this->session->userdata['logged_in']['position'] == "admin") { ?>
                          <a href="<?php echo base_url('users/enable_user/'.$row->user_id) ?>" class="btn btn-success">
                                    <i class="fas fa-toggle-off"></i>
                                    </a>
                              <?php } ?>

                                  <?php } ?>

                               <?php endif; ?>
                                </td>

<div class="modal fade" id="viewModal<?php echo $row->user_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View User Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                               <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                   <form>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Firstname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="fname" value="<?php echo $row->users_firstname; ?>" class="form-control" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                      <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Lastname</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" value="<?php echo $row->users_lastname; ?>" name="lname" class="form-control" readonly/>
                                                                </div> 
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Address</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="address" class="form-control" value="<?php echo $row->address; ?>" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Email</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="email" name="email" class="form-control" value="<?php echo $row->email; ?>" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Position</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="email" name="email" class="form-control" value="<?php echo $row->position; ?>" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Username</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="text" name="username" class="form-control" value="<?php echo $row->username; ?>" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Password</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="password" name="password" class="form-control" value="<?php echo $row->password; ?>" readonly />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br/>
                                                 <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    <label class="login2 pull-right pull-right-pro">Status</label>
                                                                </div>

                                                                <div class="col-lg-8">

                              <?php if($row->status  == 0) { ?>

                            <h4> <span class="badge badge-success">Active</span></h4>

                         <?php } ?>

                         <?php if($row->status == 1) { ?>

                           <h4> <span class="badge badge-warning">In-active</span></h4>

                         <?php } ?>
                         </div>
                                                            </div>
                                                        </div>
                                                   </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
    </div>
  </div>
</div>



                 <?php endforeach; ?>
                        <?php else: ?>
                        <tr>
                        <td>No Records Found</td>
                        </tr>
                        <?php endif; ?>
                        </tr>
                   
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>


</script>

  <script type="text/javascript">

    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 3000);


  </script>

  <?php $this->load->view('users/add-user-modal'); ?>