    <h1><center><font color="##ffffff">Account Settings</font></center></h1>
<?php echo form_open('users/update_account'); ?>
<div class="container" style="margin-top:30px;">
<div class="row">
  <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="panel panel-default">
      <div class="panel-body">
          <div class="form-group">
          <?php foreach($value as $value): ?>
            <input type="hidden" name="id" value="<?php echo $this->session->userdata['logged_in']['user_id'];?>">
        <label for="Firstname">First Name</label>
        <input type="text" class="form-control" name="firstname" value="<?php echo $value->users_firstname; ?>"    required/>
          </div>
    <div class="form-group">
      <label for="Lastname">Last Name</label>
      <input type="text" class="form-control" name="lastname" value="<?php echo $value->users_lastname; ?>"  required/>
    </div>
            <div class="form-group">
      <label for="Email">E-mail</label>
      <input type="email" class="form-control" name="email" value="<?php echo $value->email; ?>" required/>
    </div>
            <div class="form-group">
      <label for="Email">Address</label>
      <input type="address" class="form-control" name="address" value="<?php echo $value->address; ?>" required/>
    </div>
        <div class="form-group">
      <label for="username">Username</label>
      <input type="text" class="form-control" name="username" value="<?php echo $value->username; ?>"/>
    </div>
        <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" name="password" value="<?php echo $value->password; ?>" required/>
    </div>

    <center>
        <button type="submit" class="btn btn-primary">Update</button>
        <input type="button" class="btn btn-danger" value="cancel" onclick="window.location='<?php echo base_url()."Welcome/index"; ?>'"/>
      </center>
      </div>
      </div>
      </div>
       </div>
     </div>
     <?php endforeach; ?>
<?php echo form_close(); ?>
</div>