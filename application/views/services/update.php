
                        <div class="col-lg-12">
                            <div class="sparkline12-list shadow-reset mg-t-30">
                                <div class="sparkline12-hd">
                                    <div class="main-sparkline12-hd">
                                        <h1>Edit Service</h1>
                                    </div>
                                </div>



 <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <?php echo form_open("services/update_service/{$value->service_id}"); ?>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Service Name</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <input type="text" value="<?php echo $value->service_name; ?>" name="service_name" class="form-control"  />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Service Description</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                  <textarea rows="5" cols="30" class="form-control" name="service_description">
                                                                      <?php echo $value->service_description; ?>
                                                                  </textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                       <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Service Status</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <div class="form-select-list">
                                                                        <select class="form-control custom-select-value" name="service_status">
                                                                            <option value="1">Active</option>
                                                                            <option value="2">Inactive</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-1">
                                                                    <label class="login2 pull-right pull-right-pro">Price</label>
                                                                </div>
                                                                <div class="col-lg-2">
                                                                  <div class="input-group-prepend">

                                                  <span class="input-group-text" id="basic-addon1">₱</span>

                                                                    <input type="number"  name="service_price" class="form-control" value="<?php echo $value->service_price; ?>" />
                                                                      </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                      
 <br/>

                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-2"></div>
                                                                    <div class="col-lg-2">
                                                                        <div class="login-horizental cancel-wp pull-right">
                                                                            <a href="<?php echo base_url('services/list_services'); ?>" class="btn btn-danger">Cancel</a>
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
