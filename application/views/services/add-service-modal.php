<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Services</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                        <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <?php echo form_open('services/insert'); ?>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Name</label>
                                                                </div>
                                                                <div class="col-lg-7">
                                                                    <input type="text" name="sname" class="form-control" required>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Description</label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <textarea rows="5" cols="30" name="descript"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                       <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Status</label>
                                                                </div>
                                                                <div class="col-lg-7">
                                                                    <div class="form-select-list">
                                                                        <select class="form-control custom-select-value" name="status">
                                                                            <option value="1">Active</option>
                                                                            <option value="2">Inactive</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Price</label>
                                                                </div>
                                                                <div class="col-lg-7">
                                                                  <div class="input-group-prepend">

                                                  <span class="input-group-text" id="basic-addon1">₱</span>

                                                                    <input type="number"  name="price" class="form-control" />
                                                                      </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                      
 <br/>

                                                        <div class="form-group-inner">
                                                            <div class="login-btn-inner">
                                                                <div class="row">
                                                                    <div class="col-lg-6"></div>
                                                                    <div class="col-lg-6">
                                                                        <div class="login-horizental cancel-wp pull-right">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            <button class="btn btn-info login-submit-cs" type="submit">Save</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php echo form_close(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

      </div>
    </div>
  </div>
</div>