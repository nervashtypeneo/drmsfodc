<style type="text/css">
  
  .modal-header{
    color:#fff;
    background-color: #428bca;
  }    



</style>

        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Service List</h1>

          
                                  <?php if($success = $this->session->flashdata('success')): ?>
                                <div class="alert alert-success success-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $success; ?></strong></p>
                                </div>
                              <?php endif; ?>

                                        <?php if($error = $this->session->flashdata('error')): ?>
                                <div class="alert alert-danger alert-danger-style1">
                                    <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                                        <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                                    </button>
                                    <span class="adminpro-icon adminpro-checked-pro admin-check-sucess"></span>
                                    <p><strong><?php echo $error; ?></strong></p>
                                </div>
                              <?php endif; ?>


 <div class="card shadow mb-4">
            <div class="card-header py-3">

              <a href="" data-toggle="modal" style="text-align: center;" data-target="#addModal" class="btn btn-primary"><i class="far fa-plus-square"></i>New Service</a>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                    <tr>
                        <th class="table-active">ID</th>
                        <th class="table-success">Service Name</th>
                        <th class="table-primary">Status</th>
                        <th class="table-danger">Price</th>
                        <th class="table-warning">Action</th>

                    </tr>
                </thead>

                <tbody>


                    <?php if(!empty($value)): ?>

                     <?php foreach($value as $row): ?>   

                    <tr>
                        <td align="center" style="color: red"><?php echo $row->service_id; ?></td>

                        <td align="center"  class="table-active"><?php echo $row->service_name; ?></td>

                        <td align="center" class="table-active">

                            <?php if($row->service_status  == 1) { ?>

                             <span class="badge badge-success">Active</span>

                         <?php } ?>

                         <?php if($row->service_status == 2) { ?>

                            <span class="badge badge-active">In-active</span>

                         <?php } ?>

                        </td>

                        <td align="center"><?php echo $row->service_price; ?></td>


                        <td align="center">

                            <a href="#" data-toggle="modal" data-target="#editModal<?php echo $row->service_id; ?>" class="btn btn-info">
                            <i class="far fa-eye"></i></a> 



                            <a href="<?php echo base_url('services/edit_form/'.$row->service_id); ?>" class="btn btn-success">
                            <i class="fas fa-user-edit"></i></a> 

                            <?php if($row->service_status == 1) { ?>
                           <a href="<?php echo base_url('services/disable/'.$row->service_id); ?>" class="btn btn-danger">
                            <i class="fas fa-ban"></i></a>
                          <?php } ?>

                          <?php if($row->service_status == 2) { ?>
                           <a href="<?php echo base_url('services/enable/'.$row->service_id); ?>" class="btn btn-success">
                            <i class="fas fa-ban"></i></a>
                          <?php } ?>
                        </tr>

 <div class="modal fade" id="editModal<?php echo $row->service_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">View Services</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                                        <div class="sparkline12-graph">
                                    <div class="basic-login-form-ad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="all-form-element-inner">
                                                    <form>


                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Name</label>
                                                                </div>
                                                                <div class="col-lg-7">
                                                                    <input type="text" value="<?php echo $row->service_name; ?>" class="form-control" readonly />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Description</label>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <textarea rows="5" cols="30" name="descript" readonly="true"><?php echo $row->service_description; ?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                       <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Service Status</label>
                                                                </div>

                                                                <div class="col-lg-2">

                              <?php if($row->service_status  == 1) { ?>

                            <h4> <span class="badge badge-success">Active</span></h4>

                         <?php } ?>

                         <?php if($row->service_status == 2) { ?>

                           <h4> <span class="badge badge-warning">In-active</span></h4>

                         <?php } ?>
                         </div>
                                                            </div>
                                                        </div>

                                                        <br/>

                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2 pull-right pull-right-pro">Price</label>
                                                                </div>
                                                                <div class="col-lg-7">
                                                                  <div class="input-group-prepend">

                                                  <span class="input-group-text" id="basic-addon1">₱</span>

                                                                    <input type="number"  name="price" class="form-control" value="<?php echo $row->service_price; ?>" readonly/>
                                                                      </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                      
 <br/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
    </div>
  </div>
</div>


                      </tr>

                            <?php endforeach; ?>
                            <?php else: ?>
                            <tr>
                                <td>No Records Found</td>
                            </tr>
                            <?php endif; ?>
                            </tr>
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>


</script>

  <script type="text/javascript">

    window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 3000);


  </script>

  <?php $this->load->view('services/add-service-modal'); ?>

