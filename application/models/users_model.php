<?php 

 class users_model extends CI_Model{

 	public function insert($data){

 		$this->db->insert('users',$data);


 			if ($this->db->affected_rows() > 0) {

			return true;
			
			}else{

			return false;

			}


 	}

 	public function get_users(){


 		$this->db->select('*');
 		$this->db->from('users');
 		$this->db->where('user_level',2);
 		$query = $this->db->get();

 		if($query->num_rows()>0){

			return $query->result();

		}else{

			return false;
		}


 	}

 	 	public function get_all_users(){

 		$this->db->select('*');
 		$this->db->from('users');
 		$query = $this->db->get();

 		if($query->num_rows()>0){

			return $query->result();

		}else{

			return false;
		}


 	}

 	public function get_acc_by_id($user_id){

 		$query = $this->db->get_where('users',array('user_id' => $user_id));

			if($query->num_rows()>0){

			return $query->row();

			}
	}

	public function update_user($user_id,$data){

		$this->db->where('user_id',$user_id);

		if($this->db->update('users',$data)){

			return true;

		}else{
			
			return false;
		}


	}

	public function get_user($user_id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			 {
				return $query->result();
			}
	}





 }


?>