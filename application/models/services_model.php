<?php

class services_model extends CI_Model{

	public function insert($data){

		$this->db->insert('services',$data);

		 	if ($this->db->affected_rows() > 0) {

			return true;
			
			}else{

			return false;

			}

	}

	public function get_services(){

 		$this->db->select('*');
 		$this->db->from('services');


 		$query = $this->db->get();

 		if($query->num_rows()>0){

			return $query->result();

		}else{

			return false;
		}

	}

		public function get_all_services(){

 		$this->db->select('*');
 		$this->db->from('services');
 		$this->db->where('service_status',1);

 		$query = $this->db->get();

 		if($query->num_rows()>0){

			return $query->result();

		}else{

			return false;
		}

	}

	public function get_service_by_id($service_id){

		$query = $this->db->get_where('services',array('service_id' => $service_id));

			if($query->num_rows()>0){

			return $query->row();

			}

	}

	public function update_data($service_id,$data){

		$this->db->where('service_id',$service_id);

		if($this->db->update('services',$data)){

			return true;
			
		}else{

			return false;
		}
	}
}
?>