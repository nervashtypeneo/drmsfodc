<?php

class patient_model extends CI_Model{

	public function insert($data){

			$this->db->insert('patient',$data);

		 	if ($this->db->affected_rows() > 0) {

			return true;
			
			}else{

			return false;

			}

	}
	public function get_patient(){

		 $this->db->select('*');
 		$this->db->from('patient');

 		$query = $this->db->get();

 		if($query->num_rows()>0){

			return $query->result();

		}else{

			return false;
		}
	}

	public function get_selected_patient($patient_id){

			$query = $this->db->get_where('patient',array('patient_id' => $patient_id));

			if($query->num_rows()>0){

			return $query->row();

			}
	}

	public function update_data($patient_id,$data){

		$this->db->where('patient_id',$patient_id);

		if($this->db->update('patient',$data)){

			return true;
			
		}else{

			return false;
		}

	}

}














 ?>