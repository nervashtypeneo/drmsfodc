<?php
class records_model extends CI_Model{

	public function insert_toserve($data){

		$this->db->insert('records',$data);

		$id =$this->db->insert_id();
		return(isset($id)) ? $id :FALSE;

	}
	public function insert_records_detail($records_details){

		$this->db->insert('records_details',$records_details);
	}

	public function get_all_service($stat){

		$query = $this->db->query("SELECT * FROM records LEFT JOIN patient ON records.id_patient = patient.patient_id LEFT JOIN users ON records.id_user = users.user_id WHERE records_status='$stat' ORDER BY records_id DESC");
		if($query->num_rows()>0){
			return $query->result();
		}else{
			return false;
		}
	}

	public function get_records_detail($record_detail){

		$this->db->select('*');
		$this->db->from('records_details');
		$this->db->join('services','service_id=id_service');
		$this->db->join('records','records_id=details_id');
		$this->db->join('patient','patient_id=id_patient');
		$this->db->where('records_id',$record_detail);
		$query = $this->db->get();
		return $query->result();

	}

	public function confirm_service($details_id,$status){

		$this->db->where('records_id',$details_id);
		$this->db->update('records',$status);

	}


	public function get_precords($patient_id){
		$this->db->from('records');
		$this->db->JOIN('patient','id_patient=patient_id','LEFT');
		$this->db->JOIN('users','id_user=user_id','LEFT');
		$this->db->where('patient_id',$patient_id);
		$this->db->where('records_status !=',0);

		$query=$this->db->get();
		if($query->num_rows()>0)
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}

	public function count_serving()
	{
		$this->db->select('count(records_status) as count_total');
		$this->db->from('records');
		$this->db->where('records_status',2);
		$query = $this->db->get();
		$row = $query->row_array();
		if(isset($row))
		{
			return $row['count_total'];
		}
		else
		{
			return false;
		}
	}
	public function count_topay()
	{
		$this->db->select('count(records_status) as count_total');
		$this->db->from('records');
		$this->db->where('records_status',3);
		$query = $this->db->get();
		$row = $query->row_array();
		if(isset($row))
		{
			return $row['count_total'];
		}
		else
		{
			return false;
		}
	}

		public function count_toserve()
	{
		$this->db->select('count(records_status) as count_total');
		$this->db->from('records');
		$this->db->where('records_status',1);
		$query = $this->db->get();
		$row = $query->row_array();
		if(isset($row))
		{
			return $row['count_total'];
		}
		else
		{
			return false;
		}
	}


}?>